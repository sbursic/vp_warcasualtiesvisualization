


////////// DEFINING A SCREEN SIZE /////////////////////
var screenWidth = 1300;
var screenHight = 600;
//////////////////////////////////////////////


//////////////////  CREATING ZOOM  //////////////////////////
var zoom = d3.zoom()  // Izvor gdje sam dobio ideju https://www.d3-graph-gallery.com/graph/interactivity_zoom.html
.scaleExtent([1, 20])  
.extent([[0, 0], [screenWidth, screenHight]])
.translateExtent([[0, 0], [screenWidth, screenHight]])
.on("zoom", updateChart);

let noZoom = d3.select('#svg1')
        .attr('width', screenWidth)
        .attr('height', screenHight)
        .style('background-color', 'silver');
        
////////////////////////////////////////////////////

let svg = d3.select('#svg1')       
        .attr('width', screenWidth)
        .attr('height', screenHight)
        .style('background-color', 'silver')
        .call(zoom)
        .append("g");
        
        
///////////// TIMELINE ///////////////////////////

var timeline = svg.append('line')
        .style("stroke", "black")
        .style("stroke-width", 4)
        .attr("x1", 0)
        .attr("y1", screenHight/2)
        .attr("x2", screenWidth)
        .attr("y2", screenHight/2);
///////////////////////////////////////////  


/////////////////// FOR COLORING THE CIRCLES ////////////////////////////////
var myColor = d3.scaleSequential().domain([00,1000000])   // Izvor: https://github.com/d3/d3-scale-chromatic 
       .interpolator(d3.interpolatePuRd);
///////////////////////////////////////////////////////

///////////////////// TITLE AND DESCRIPRION /////////////////////////////7 
noZoom.append("text")
          .attr("x", screenWidth*0.02)   
          .attr("y", screenHight*0.08)
          .style("font-size", "26px")
          .text("War Casualties Visualization" );  

noZoom.append('circle')
          .style("stroke","black")
          .style("stroke-width",0.1)
          .style("fill", myColor(0))
          .attr("cx",screenWidth*0.02)
          .attr("cy",screenHight*0.15)
          .attr("r",20);

noZoom.append("text")
          .attr("x", screenWidth*0.05)   
          .attr("y", screenHight*0.15)
          .text("Minimum war mortality rate" ); 

        
noZoom.append('circle')      
          .style("stroke","black")
          .style("stroke-width",0.1)
          
          .style("fill", myColor(1000000))
          .attr("cx",screenWidth*0.02)
          .attr("cy",screenHight*0.22)
          .attr("r",20);

noZoom.append("text")
          .attr("x", screenWidth*0.05)   
          .attr("y", screenHight*0.22)
          //.style("font-size", "26px")
          .text("Maximum war mortality rate" ); 

noZoom.append("text")
          .attr("x", screenWidth*0.02)   
          .attr("y", screenHight*0.29)
          //.style("font-size", "26px")
          .text("Circle size shows the number of casualties " ); 

////////////////////////////////////////////////////////////////////////////////////////////   
   

var wName = "";
var wCas= "";
var wTime= "";


d3.json("warCasualtiesData_full.json", function(data) {
                

  var TimelineLines=12;
  let firstYear = 3000;
  let lastYear= -3000;

  
  //var counterCircles = 0;
  var warCounter = 0;
  

/////////////////////     SETTING FIRST AND LAST YEAR ON THE TIMELINE  //////////////////////////////////////
  data.forEach(element => {
    warCounter++;
    //console.log(element.Time_start);
    if(element.Time_start < firstYear) firstYear=element.Time_start;
    if (element.Time_end > lastYear) lastYear= element.Time_end;
                  
  });


  let countToZero=0;
  if( firstYear<0) countToZero = 0 - firstYear;

             
  var TimelineAllYears=   Math.abs(lastYear)+ Math.abs(firstYear);             
  let TimelineLineValue = Math.floor(TimelineAllYears/(TimelineLines-1));
         
  var i;
  var timelineStart= 50;
  var placeLinex=timelineStart;     
  let TimelineCurrent=firstYear;
  /////////////////////////////////////////////////////////////



/////////////////////// TAGS (year numbers) ON THE TIMELINE /////////////////////////////////
  for (i = 0; i < TimelineLines; i++) {
    svg.append('line')
          .style("stroke","white")
          .style("stroke-width", 4)
          .attr("x1", placeLinex)
          .attr("y1", (screenHight/2)+10)
          .attr("x2", placeLinex)
          .attr("y2", (screenHight/2)-10);

    svg.append('text')      
          .style('font-size',"14px")
          .attr("x",placeLinex-30)
          .attr("y",(screenHight/2)+30)
          .text(TimelineCurrent);

    TimelineCurrent+=TimelineLineValue;
    placeLinex+=(screenWidth-100)/(TimelineLines-1);
}


var yearToTimeline = (screenWidth-100)/(TimelineAllYears);

///////////////////////////////////////////////////////////////


let isActiveFlag=0;

  
/////////////// CALCULATIONS   ////////////////////////////////////
var Casualties_mid = [];
var logr = [];
var warDuration = [];
var casualtiesRate= [];
var warBegin = [];
var name =[];
var time =[];
var casualties =[];
var warEnd = [];

data.forEach(element => {
  let tempC,tempR,tempD,tempW;

  //SREDNJI BROJ ZRTAVA
  tempC = (element.Casualties_max+ element.Casualties_min)/2;
  Casualties_mid.push(tempC);

  // RADIUSI KRUGOVA
  //tempR = ((Math.log10(tempC))*15)-40;  //Formula s kojom se balansiraju veličine krugova
    tempR = ((Math.log10(tempC))*10)-30;

  logr.push(tempR);

  // TRAJANJE RATA
  if ((element.Time_end - element.Time_start)==0)
    tempW=1;
  else 
    tempW = (element.Time_end - element.Time_start);

    warDuration.push(tempW);

  // SMRTNOST RATA
  tempD = tempC/tempW;
  casualtiesRate.push(tempD);

  warBegin.push(element.Time_start);
  name.push(element.Name);
  time.push(element.Time);
  casualties.push(element.Casualties);
  warEnd.push(element.Time_end);



});
//////////////////////////////////////////////////////////////

var war;
var dummyData = [];

  ////////////////////////    GETTING DATA FOR EVERY WAR   ///////////////////////////////////////7
  for (war = 0; war < warCounter; war++){

      let feed = {number : war, 
                  radius :logr[war], 
                  casRate : casualtiesRate[war],
                  warStart :warBegin[war],
                  warName :name[war],
                  warTime : time[war],
                  warCasualties : casualties[war],
                  warEndYear : warEnd[war] };
     //let feed = war;
      dummyData.push(feed);

  }
  console.log(dummyData);
///////////////////////////////////////////////////////////


//////////////////////     MOUSE OVER A CIRCLE      ///////////////////////////////7
  function handleMouseOver(warName, warTime,warCasualties, warStart, warEndYear,cx,cy) {
             
    if(isActiveFlag==1){
      isActiveFlag=0;
      textName.remove();
      textTime.remove();
      textCas.remove();
              
    }
                      
    textName=  noZoom.append("text")
                .raise()
                .attr("x", screenWidth*0.01)   
                .attr("y", screenHight*0.75)
                .text(warName );  
 
    textTime=  noZoom.append("text")
                .raise()
                .attr("x", screenWidth*0.01)   
                .attr("y", screenHight*0.85)
                .text("Casualties: "+warCasualties );  
 
 
    textCas=  noZoom.append("text")
                .raise()
                .attr("x", screenWidth*0.01)   
                .attr("y", screenHight*0.80)
                .text("Time: "+warTime );  
            
 
    timeline_warLenght =  svg.append("line")
             .style("stroke", "red")
             .style("stroke-width", 4)
             .attr("x1", ((yearToTimeline*((warStart)+countToZero))+timelineStart))
             .attr("y1", (screenHight/2))
             .attr("x2", ((yearToTimeline*((warEndYear)+countToZero))+timelineStart))
             .attr("y2", screenHight/2);


             
    circleTimeline_line= svg.append('line')
             .style("stroke", "black")
             .style("stroke-width", 1)
             .attr("x1", cx)
             .attr("y1", cy)
             .attr("x2", ((yearToTimeline*((warStart)+countToZero))+timelineStart))   
             .attr("y2", (screenHight/2));        
             
  }
///////////////////////////////////////////////////////////////////

/////////////////////// MOUSE OUT OF A CIRCLE ////////////////////////////////////////////
  function handleMouseOut() {

    if( isActiveFlag==0) {
     // d3.select(this)
       //      .style("fill",myColor(casRate));
 
            
      textName.remove();
      textTime.remove();
      textCas.remove();
      timeline_warLenght.remove();
      circleTimeline_line.remove();
            
    }
          
  }
 ///////////////////////////////////////////////////////

 
 





//////// INSERTING CIRCLES (EACH CIRCLE REPRESENTS A WAR)
var node=  svg
        .selectAll("circle")
        .data(dummyData)  
        .enter()
        .append("circle")  
        .style("stroke","black")
        .style("stroke-width",0.1)
        .lower()
        //.style("fill", "blue")
        .style("fill", function(d){return (myColor(d.casRate))})
        .attr("cx",function(d){return ((yearToTimeline*((d.warStart)+countToZero))+timelineStart)})
        .attr("cy",screenHight/2)
        .attr("r" ,function(d){return (d.radius)})
        .on("mouseover", function(d){handleMouseOver(d.warName, d.warTime, d.warCasualties, d.warStart, d.warEndYear,(d3.select(this).attr("cx")),(d3.select(this).attr("cy")))
        }
        )
        .on("mouseout", function(d){handleMouseOut()});
        

//////////////////////////////////////////////////////////////////////////////////////////////////////    






/////////////////////// CRETING FORCES AND BOUNDING BOXES /////////////////////////////////////////////////////
   // Izvor : https://www.d3indepth.com/force-layout/
   
  var simulation = d3.forceSimulation()
      .force('x', d3.forceX().x(function(d){return ((yearToTimeline*((d.warStart)+countToZero))+timelineStart)}))
  
      .force('y', d3.forceY().y(function(d) {
           return screenHight/2;
                      }))
     
      .force("charge", d3.forceManyBody().strength(0.5)) 
      .force("collide", d3.forceCollide().radius(function(d) { return d.radius + 5; }).iterations(2));
      
        simulation
        .nodes(dummyData)
        .on("tick", function(d){
          node
              .attr("cx", function(d){ return d.x; })   
              .attr("cy", function(d){ return d.y; })
              
        })
        .on("tick", tickActions );



        function tickActions() {
          
            node
              .attr("cx", function(d) { return d.x = Math.max(d.radius, Math.min(screenWidth - d.radius, d.x)); })
              .attr("cy", function(d) { return d.y = Math.max(d.radius, Math.min(screenHight - d.radius, d.y)); });
   
      } 
           
 //////////////////////////////////////////////////////////////////////////////////////

                 
});




///////////////// ZOOM UPDATE //////////////////////////////////////////
function updateChart() {   

  svg.attr("transform", d3.event.transform);
                     
}

/////////////////////////////////////////////////////////////////



 
        
      


       
